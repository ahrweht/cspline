.. CSpline documentation master file, created by
   sphinx-quickstart on Fri Sep 16 14:56:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CSpline's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2
   
.. automodule:: CSpline
.. autoclass:: CSpline
   :members:
   :private-members:
   :special-members:

We also have a test script: testCSpline.py.  Running this script will produce a random spline with control points within 10 units of each other on each axis and knots within 10 units of each other.  It will evaluate the spline at 10 random points in the acceptable interval using the basic functions method as well as deBoors algorithm and compare the results to test for accuracy.  It will also test the endpoints of the interval.  Afterwards it will evaluate both methods 1000 times at the same random point and return the average time to execute for each as a speed test.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

