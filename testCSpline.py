"""
@author: Arved, Axel, Dara, Martin, Lingzhi
"""

from  scipy import *
from  pylab import *
from CSpline import *
import random
import time

#initialize D & U.  Each control point is a random point within 10 of the previous.
#Each knot is within 10 of the previous knot.
D = [[random.uniform(-10,10), random.uniform(-10,10)]]
U = [0]

for i in range(0,15):
	D.append([D[i][0] + random.uniform(-10,10), D[i][1] + random.uniform(-10,10)])
	U.append(U[i] + random.uniform(0,10))

U.append(U[len(U) - 1] + random.uniform(0,10))
U.append(U[len(U) - 1] + random.uniform(0,10))

#make arrays
U = array(U)
D = array(D)

spline = CSpline(D,U)

#Print (u and d values for debugging purposes)
print ("u values: ", U)
print ("d values: ", D)

#Randomly test 10 points in the acceptable interval.  If results for the two methods
#match, then test was a success.  Else, test is a failure and both outputs are reported
for i in range(0,10):
	#randomly pick u
	u = random.uniform(U[2], U[len(U) - 3])
	print ("Testing u = ", u)
	#get deBoors result via call
	deBoors = spline(u)
	#get Basic Function result via recursive method and summation
	basicFunc = 0
	I = spline.__findInt__(u)
	for j in range(I - 2, I + 2):
		basicFunc += D[j] * CSpline.BasicFunctionRec(U, u, j)

	#test for equality
	if basicFunc.all() == deBoors.all():
		print ("Success!  Result: ", basicFunc )
	else:
		print ("Failure!\n Basic Function Result: ", basicFunc, "\ndeBoors Result: ", deBoors)

#test edges
u = U[2]
print ("Testing second knot")
#get deBoors result via call
deBoors = spline(u)
#get Basic Function result via recursive method and summation
basicFunc = 0
I = spline.__findInt__(u)
for j in range(I - 2, I + 2):
	basicFunc += D[j] * CSpline.BasicFunctionRec(U, u, j)
#test for equality
if basicFunc.all() == deBoors.all():
	print ("Success!  Result: ", basicFunc )
else:
	print ("Failure!\n Basic Function Result: ", basicFunc, "\ndeBoors Result: ", deBoors)

#test edges
u = U[len(U) - 3]
print ("Testing second to last knot")
#get deBoors result via call
deBoors = spline(u)
#get Basic Function result via recursive method and summation
basicFunc = 0
I = spline.__findInt__(u)
for j in range(I - 2, I + 2):
	basicFunc += D[j] * CSpline.BasicFunctionRec(U, u, j)
#test for equality
if basicFunc.all() == deBoors.all():
	print ("Success!  Result: ", basicFunc )
else:
	print ("Failure!\n Basic Function Result: ", basicFunc, "\ndeBoors Result: ", deBoors)
#Speed test.  Find better average over 1000 trials
#Decide on random point to test
toTest = random.uniform(U[2], U[len(U) - 3])

#test deBoors
t0 = time.clock()
for trial in range(0,1000):
	resultDB = spline(toTest)
t1 = time.clock()
avgDeBoorsTime = (t1 - t0) / 1000

#test basic functions
t0 = time.clock()
for trial in range(0,1000):
	resultBasic = 0
	I = spline.__findInt__(u)
	for j in range(I - 2, I + 2):
		resultBasic += D[j] * CSpline.BasicFunctionRec(U, u, j)
t1 = time.clock()
avgBasicTime = (t1 - t0) / 1000

#print (times and results)
print ("deBoors: Average time (ms) = ", avgDeBoorsTime * 1000, ".")
print ("Basic: Average time (ms) = ", avgBasicTime * 1000, ".")