# -*- coding: utf-8 -*-
"""
Created on Wed Sep  7 09:28:50 2016
@author: Arved, Axel, Dara, Martin, Lingzhi
"""
from  scipy import *
import  scipy.linalg as lnlg
from  pylab import *

class CSpline:
    """
    Class for generation of cubic splines
    """
    def __init__(self, D, U = None):
        """
        Arguments:
        D = (k-2)*2 array or list of de boore points
        U = k*1 array or list, default: U = [ 0, 1, 2, 3, .... , k]
        """
        #check input 
        if (isinstance(D,list)):
            D = array(D)
        Ddim1,Ddim2 = shape(D)
        if (Ddim2 != 2):
            print('EXCEPTION')
        
        if (U == None):
            U = arange(Ddim1 + 2)
        if (isinstance(U,list)):
            U = array(U)
            
        (Udim1,) = shape(U)
        
        if (Udim1 != Ddim1 + 2):
            print('EXCEPTION')            

        self.__D = D
        self.__U = U
    
    @property 
    def U(self):
        """
        Vector of u
        """
        return self.__U

    @U.setter
    def U(self, U):
        """
        changes in shape of U not possible
        """
        if (isinstance(U, list)):
            U = array(U)
        if (U.shape != self.__U.shape):
            print('EXCEPTION')
        else:
            self.__U = U
    
    @property
    def D(self):
        """
        Matrix of D
        """
        return self.__D

    @D.setter
    def D(self, D):
        """
        changes in shape of D not possible
        """
        if (isinstance(D, list)):
            D = array(D)
        if (D.shape != self.__D.shape):
            print('EXCEPTION')
        else:
            self.__D = D

    def __call__(self, u): # evaluate function with De Boore
        """
        evaluates spline at position u
        """
        if(type(u) is ndarray):
            result = zeros((len(u), 2))
            for i in range(len(u)):
                currentU = u[i]
                I = self.__findInt__(currentU)
                result[i] = self.__recD__(self.D[I-2:I+2], I, currentU)
            return result
        else:    
            I = self.__findInt__(u)
            return self.__recD__(self.D[I-2:I+2], I, u)

    def plot(self, resolution = 1/100):
        """
        plots spline, resolution = step size between u
        """
        # plot d
        X = []
        Y = []
        for d in self.D:
            X.append(d[0])
            Y.append(d[1])
        fig = figure()
        plot(X, Y, 'rx--')
        # plot spline        
        for i in range(len(self.U) - 5):
            leftU=self.U[i + 2]
            rightU = self.U[i + 3] - resolution
            
            plotIntervall = arange(leftU, rightU, resolution)
            points = self(plotIntervall)
            X = []        
            Y = []
            for point in points:
                X.append(point[0])
                Y.append(point[1])
            plot(X,Y,'b-')
            plot(self(leftU)[0],self(leftU)[1],'b*-')
        show()
        
    def __merge__(self, u, d_top, d_bot, I_left, I_right):
        """
        Merges two blossoms for a step of deBoors algorithm
        """
        alpha = self.__alpha__(I_left, I_right, u)
        return alpha * d_top + (1 - alpha) * d_bot

    def __recD__(self, lst, I, u):
        """
        the blossom recursion for finding s(u)
        lst containsthe values of the blossoms
        the blossoms should initially be d_(I-2)...d_(I+1)
        u is the value at which the function should be evaluated
        I should be chosen such that U[I] <= u < U[I+1]
        """
        length = len(lst)
        if length == 2: # find answer
            return self.__merge__(u, lst[0], lst[1], I, I+1)
        else:           # call self with smaller list
            newList = []    
            for num in range(1, length):
                newList.append(self.__merge__(u, lst[num-1], lst[num], I-length+num+1, I+num))
            return self.__recD__(newList, I, u)
    
    
    def __findInt__(self,u):
        """
        finds the hot intervall
        """
        i=(self.__U > u).argmax()
        if i == len(self.__U) - 2:
            return i - 2
        return i-1
    
    
    def __alpha__(self,I_left, I_right,u):
        """
        determines alpha
        """
        a = (self.__U[I_right]-u)/(self.__U[I_right]-self.__U[I_left])
        return a    

    @classmethod
    def BasicFunctionRec(self, nodes, x, i, k = 3):
        """
        This method returns the value of the basicfunction dependent on a list of nodes,
        a variable x and a index i.
        Optionally you could insert a value k which indicates the recursion depth of
        the algorithm and therfore defines the polynomial degree of the basicfunction.
        Since we are interested in a cubic polynom, this value is set to 3 by default.
        The algorithm contains a recursive part, which computes the function in the
        usual case, as well as some special cases i.e. the first and last function.
        """
        
        if i == 0: # special case for N_0^3 since it wouldn't work with the recursion
            try:
                if 3 == (nodes > x).argmax():
                    return ((nodes[i+3] - x)**3)/ ((nodes[i+3] - 
                            nodes[i]) * (nodes[i+3] - nodes[i+1]) * (nodes[i+3] - 
                            nodes[i+2]))
                else:
                    return 0
            except ZeroDivisionError:
                return 0
                        
        
        
        if i == (len(nodes) - 3):   # special case for N_{K-2}^3 since it wouldn't
            try:                    # work with the recursion 
                if x == nodes[i]:
                    return (((x -nodes[k-1])**2) * (nodes[k+1] - x))/ ((nodes[k+2] - 
                            nodes[k+1]) * (nodes[k+1] -nodes[k-1]) *  (nodes[k+1] - 
                            nodes[k]))
                elif i == (nodes > x).argmax():
                    if ((nodes[k+2] - nodes[k-1]) * (nodes[k+1] - nodes[k-1]) *
                            (nodes[k] - nodes[k-1])) == 0:
                                return 0
                    return ((x - nodes[k-1])**3)/ ((nodes[k+2] -
                            nodes[k-1]) * (nodes[k+1] - nodes[k-1]) * (nodes[k] - 
                            nodes[k-1]))
                else:
                    return 0
            except ZeroDivisionError:
                return 0
                
        if k == 0:                      # the usual recursion
            if nodes[i-1] == nodes[i]:
                return 0
            elif i == (nodes > x).argmax():
                return 1
            else:
                return 0
        else: 
            try:
                if ((x * (nodes[i+k-1] - nodes[i-1]) - nodes[i-1])) == 0:
                    if ((nodes[i+k] - x) - (nodes[i+k] * nodes[i])) == 0:
                        return 0
                    else:
                        return ((nodes[i+k] - x)/(nodes[i+k] - nodes[i])) * self.BasicFunctionRec(nodes, x, i+1, k-1)
                elif  (nodes[i+k] - x) == 0:
                    return ((x - nodes[i-1])/ (nodes[i+k-1] - nodes[i-1])) * self.BasicFunctionRec(nodes, x, i, k-1)
                else:
                    return ((x - nodes[i-1])/ (nodes[i+k-1] - nodes[i-1])) * self.BasicFunctionRec(nodes, x, i, k-1)\
                            + ((nodes[i+k] - x)/(nodes[i+k] - nodes[i])) * self.BasicFunctionRec(nodes, x, i+1, k-1)   
            except ZeroDivisionError:
                return 0

    @classmethod
    def interpolation(cls, points, U):
        """
        work in progress
        interpolation class method
        method for making a spline which passes through some given data points
        points is a list containing the points [[x_0,... x_L],[y_0,... y_L]]
        U is a list that contains the gridpoints [u_0,... u_(L+2)]
        for the system to be solvable the first and last three gridpoints
        need to have multiplicity three
        """
        U = array(U)
        K = len(U) - 1
        if not(U[0]==U[1]==U[2] and U[K-2]==U[K-1]==U[K]):
            print('EXCEPTION')
            return
        greville = []
        for num in range(0, K-1):
            greville.append((U[num]+U[num+1]+U[num+2])/3)
        A = np.zeros((K-1, K-1))
        for i in range(0, K-1):
            for j in range(0, K-1):
                if abs(i-j)<5:    
                    A[i, j] = CSpline.BasicFunctionRec(U, greville[i], j)
                    #print(greville[i], j, A[i, j])
        B = array(points)
        #print(shape(A))
        #print(shape(B))
        D = solve(A,B)
        #D = lnlg.solve_banded(4, A, B)
        return cls(D, U)

    def calculateBasicFunction(self, index ):
        """
        In this little method one can get a specific cubic basicfunction for the spline
        indicated by the variable index. The method itself uses the recursive
        method above.
        """
        return (lambda x: self.BasicFunctionRec(self.U, x, index))

    def Compare(self, variable):
        """
        To be able to test the De-Boor-Algorithm for accuracy, one could use this method
        to compare the result of the De-Boor-Algorithm to the result obtained by
        the linear combination of the basicfunctions.
        """
        I = self.__findInt__(variable) # the value 
        s = 0        
        
        for i in range(I-2, I+2):
            s += self.D[i] * self.BasicFunctionRec(self.U, variable, i)
            
        print("s(u) = sum(d_i * N_i (u)): ", s)
        print("De-Boor-Algorithm-Value: ", self(variable))

        

    
